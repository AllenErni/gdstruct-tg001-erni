#include <iostream>
#include<string>
#include"time.h"
#include<vector>
#include"UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

int main()
{
	srand(time(NULL));
	// Input Size
	int a;
	cout << "Input size: ";
	cin >> a;
	system("cls");
	// placing elements
	OrderedArray<int>numzz(a);
	UnorderedArray<int> numz(a);
	for (int i = 0; i < a; i++)
	{
		int b;
		b = rand() % 100;
		numz.push(b);
		numzz.push(b);
	}

	//output elements

	int y = 0;

	while (y == 0) {
		cout << "Unordered Elements: ";
		for (int i = 0; i < numz.getSize(); i++)
		{
			cout << numz[i] << "  ";
		}
		cout << " " << endl;
		cout << "Ordered Elements:   ";
		for (int i = 0; i < numzz.getSize(); i++)
		{
			cout << numzz[i] << "  ";
		}
		cout << " " << endl;
		int choice;
		cout << "What do you want to do?" << endl;
		cout << "[1] Search" << endl;
		cout << "[2] Remove" << endl;
		cout << "[3] Expand" << endl;
		cout << "[4] Exit" << endl;
		cin >> choice;
		cout << " " << endl;
		if (choice == 1)
		{
			// binary search
			cout << "Find element: ";
			int u;
			cin >> u;
			numzz.binarySearch(u);
			numz.seekndestroy(u);
			system("pause");
		}
		if (choice == 2)
		{
			// Removal
			int f;
			cout << "Remove: ";
			cin >> f;
			numz.remove(f);
			numzz.remove(f);
		}
		if (choice == 3)
		{
			// expand
			cout << "How many elements would you like to add: ";
			cin >> a;
			for (int i = 0; i < a; i++)
			{
				int b;
				b = rand() % 100;
				numz.push(b);
				numzz.push(b);
			}
		}
		if (choice == 4)
			y = 1;

		system("cls");
	}
}

