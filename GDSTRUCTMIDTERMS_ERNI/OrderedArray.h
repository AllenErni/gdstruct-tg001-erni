#pragma once
#include <assert.h>
#include <iostream>
#include<string>
#include<conio.h>

using namespace std;

template <class T>

class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0), mSearch(0)
	{
		if (size)
		{
			mMaxSize = size;

			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);

			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	//virtual ~UnorderedArray()
	//{
	//	if (mArray != NULL)
	//	{
	//		delete[] mArray;
	//		mArray = NULL;
	//	}
	//}


	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
		sorts();
	}

	//binary search

	void binarySearch(T x)
	{
		bool found = false;
		int mid = 0;
		
		int  l = 0;
		int  r = mNumElements -1;
		while (r >= l) {
			mid = l + (r - l) / 2;
			if (mArray[mid] == x)
			{
				cout << "Ordered Array index: " << mid << endl;
				found = true;
			}
			if (mArray[mid] < x)
				l = mid + 1;
			else
				r = mid - 1;
		}
		if (found == false)
			cout << "Element not found." << endl;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize)
			return;

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	int Search()
	{
		return mSearch;
	}

	int SearchFail()
	{
		return -1;
	}


private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;
	int mSearch;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize];

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;

		return true;
	}
	virtual void sorts()
	{
		//bubble sort
		for (int i = 1; (i <= mNumElements); i++)
		{
			for (int j = 0; j < (mNumElements - 1); j++)
			{
				if (mArray[j + 1] < mArray[j])     
				{
					swap(mArray[j], mArray[j + 1]);
					
				}
			}
		}
	}
};